const mongoose = require('mongoose');
const routes = require('../routes');

const expense = mongoose.model('expense');

module.exports = {
    async index(req,res) {
        const expenses = await expense.find();//listagem

        return res.json(expenses);
    },

    async store(req,res){
        const criar = await expense.create(req.body);//criação
        
        return res.json(criar);
    },

    async update(req,res){
        const mudar = await expense.findByIdAndUpdate(req.params.id, req.body, {new:true});//mudar

        return res.json(mudar);
    },

    async destroy(req,res){
        await expense.findByIdAndRemove(req.params.id);//delete

        return res.send();
    }

};