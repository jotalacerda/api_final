const express=require('express');
const routes = express.Router();
const expenseController = require('./controller/expenseController');

//Primeira Rota
routes.get("/expense",expenseController.index);
routes.post("/expense", expenseController.store);
routes.put("/expense/:id", expenseController.update);
routes.delete("/expense/:id", expenseController.destroy);
 
module.exports = routes; 