const mongoose = require('mongoose');

const expenseSchema = new mongoose.Schema({
    gasto:{
        type: Number,
        required: true,
    },
    data:{
        type: Date,
        default: Date.now, 
    }

});

mongoose.model('expense', expenseSchema);